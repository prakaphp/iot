﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { DeviceService, AlertService } from '@app/_services';

@Component({ templateUrl: 'shipment.component.html' })
export class ShipmentComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
	user = {"username": ""};

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private deviceService: DeviceService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
		this.user = JSON.parse(sessionStorage.getItem('user'));
        this.form = this.formBuilder.group({
            device_id: ['', Validators.required],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.deviceService.getById(this.f.device_id.value)
            .pipe(first())
            .subscribe(
                data => {
					sessionStorage.setItem('device_tmp', JSON.stringify({deviceId: this.f.device_id.value}));
                    this.router.navigate(["device"]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}