﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ToastrService } from "ngx-toastr";
import { DatePipe } from '@angular/common';

import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

import { User } from '@app/_models';
import { DeviceService, AlertService } from '@app/_services';

@Component({ templateUrl: 'device.component.html' })
export class DeviceComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
	obj = {};
	user = {"username": ""};
	tmp = {};
	
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private deviceService: DeviceService,
        private alertService: AlertService,
		private toastr: ToastrService,
		private datePipe: DatePipe,
		private calendar: NgbCalendar
    ) {	}

    ngOnInit() {
		this.user = JSON.parse(sessionStorage.getItem('user'));
		this.form = this.formBuilder.group({
            device_id: [JSON.parse(sessionStorage.getItem('device_tmp'))['deviceId'], Validators.required],
            shipment_id: ['', Validators.required],
            carrier: ['', Validators.required],
            frequency: ['', [Validators.required, Validators.min(15), Validators.max(1440)]],
			startDate: [this.calendar.getToday(), Validators.required],
			startTime: [{hour: 0, minute: 0}, Validators.required],
			endDate: [this.calendar.getToday(), Validators.required],
			endTime: [{hour: 23, minute: 59}, Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }
	
	setTime(time, field) {
		this.tmp[field] = time;
	}

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
		this.obj['deviceid'] = this.f.device_id.value;
		this.obj['shipmentid'] = this.f.shipment_id.value;
		this.obj['carrier']	= this.f.carrier.value;
		this.obj['frequency'] = this.f.frequency.value;
		
		this.tmp['date'] = this.f.startDate.value;
		this.tmp['time'] = this.f.startTime.value;
		this.obj['starttime'] = this.tmp['date']['year'] + '-' + this.pad(this.tmp['date']['month'], 2) + '-' + this.pad(this.tmp['date']['day'], 2) + ' ' + this.pad(this.tmp['time']['hour'], 2) + ':' + this.pad(this.tmp['time']['minute'], 2) + ':00';
		
		this.tmp['date'] = this.f.endDate.value;
		this.tmp['time'] = this.f.endTime.value;
		this.obj['endtime'] = this.tmp['date']['year'] + '-' + this.pad(this.tmp['date']['month'], 2) + '-' + this.pad(this.tmp['date']['day'], 2) + ' ' + this.pad(this.tmp['time']['hour'], 2) + ':' + this.pad(this.tmp['time']['minute'], 2) + ':59';
		this.obj['custid'] = JSON.parse(sessionStorage.getItem('user'))['custid'];
		
        this.deviceService.save(this.obj)
            .pipe(first())
            .subscribe(
                data => {
					sessionStorage.removeItem('device_tmp');
					this.toastr.success(
						'<span data-notify="icon" class="nc-icon nc-check-2"></span><span data-notify="message"><b>Shipment Information</b> - information has been saved successfully!.</span>',
						"",
						{
							timeOut: 5000,
							closeButton: true,
							enableHtml: true,
							toastClass: "alert alert-success alert-with-icon",
							positionClass: "toast-top-right"
						}
					);
					setTimeout(() => {
						this.router.navigate(["/"]);
					}, 6000);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
	
	pad(num, size) {
		num = num.toString();
		while (num.length < size) num = "0" + num;
		return num;
	}
}