﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from "ngx-toastr";
import { DatePipe } from '@angular/common';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppRoutingModule } from './app-routing.module';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppComponent } from './app.component';
import { AlertComponent } from './_components';
import { HomeComponent } from './components/home';
import { DeviceComponent } from './components/device';
import { ShipmentComponent } from './components/shipment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//import { NgbdTimepickerMeridian } from './timepicker-meridian';
//import { NgbdTimepickerModule } from '@ng-bootstrap/ng-bootstrap/timepicker/timepicker.module';
import { NgbTimepickerModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
 
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

@NgModule({
    imports: [
        BrowserModule,
		BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
		ToastrModule.forRoot(),
		NgbModule,
		PerfectScrollbarModule,
		NgbTimepickerModule,
		NgbDatepickerModule
	],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
		DeviceComponent,
		ShipmentComponent
    ],
	exports: [
	],
    providers: [
		DatePipe,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

		// Perfect Scrollbar
		{ provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
		
        // provider used to create fake backend
        //fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };