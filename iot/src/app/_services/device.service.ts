﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class DeviceService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        
    }
	
	getById(id) {
		if(environment.production)
			return this.http.get<any>(`${environment.apiUrl}/checkdevice?deviceid=${id}`);
		else
			return this.http.get<any>(`${environment.apiUrl}/checkdevice/{id}`);
    }
	
	save(device) {
		return this.http.post<any>(`${environment.apiUrl}/adddevice`, device, httpOptions);
	}
}