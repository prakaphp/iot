﻿import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

import { AccountService } from './_services';
import { User } from './_models';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    user: User;
	public config: PerfectScrollbarConfigInterface = {};
	public link = '';
	
	@ViewChild(PerfectScrollbarComponent, { static: false }) componentRef?: PerfectScrollbarComponent;
	
    constructor(
		private accountService: AccountService,
		private router: Router
	) {
        this.accountService.user.subscribe(x => this.user = x);
    }
	
	addShipment() {
		this.link = 'shipment';
		this.router.navigate(["/shipment"]);
	}

    logout() {
        this.accountService.logout();
    }
}